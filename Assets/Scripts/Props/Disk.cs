﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.Events;

public class Disk : MonoBehaviour
{
    [Min(1)] public int Size = 2;
    public float GapWidth = 0.65f;

    public DiskState State = DiskState.Obstacle;

    public Peg LastPeg;
    public Peg CurrentPeg;

    public UnityEvent<Disk> OnStack;
    public UnityEvent<Disk> OnUnstack;

    private Rigidbody2D rb;

    private SpriteShapeRenderer spriteShapeRenderer;
    private SpriteShapeController spriteController;
    private CapsuleCollider2D[] colliders;

    // Multiple colliders within the beg activates OnTriggerEnter2D multiple times.
    private int pegTriggerCount = 0;

    private AudioSource audioSource;
    private AudioManager audioManager;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteController = GetComponent<SpriteShapeController>();
        spriteShapeRenderer = GetComponent<SpriteShapeRenderer>();
        colliders = GetComponents<CapsuleCollider2D>();
        audioSource = GetComponent<AudioSource>();
        audioManager = Singleton<AudioManager>.Instance;
        Initialize();
    }
    public void Initialize()
    {
        UpdateSize();
        SetState(State);

        if(State == DiskState.Stacked && FindPeg() is Peg foundPeg)
            SetPeg(foundPeg);
    }

    public void SetPeg(Peg newPeg)
    {
        if(CurrentPeg != newPeg)
        {
            LastPeg = CurrentPeg;
            CurrentPeg = newPeg;
        }
    }

    /// <summary>
    /// Updates the dimensions of the Disk and its colliders based on the Size field.
    /// </summary>
    public void UpdateSize()
    {
        // Set mass for physics
        float baseMass = 50f;
        float massPerSize = 10f;

        rb.mass = baseMass + massPerSize * Size;

        // Dimensions
        float baseWidth = 1.5f;
        float widthPerSize = 0.5f;
        float edgeCompensation = 0.5f; // To compensate for sprite shape overshoot

        float totalWidth = baseWidth + (widthPerSize * Size);
        float xSplinePos = (totalWidth - edgeCompensation)/2;

        // Set Sprite Sizes
        spriteController.spline.SetPosition(0, new Vector3(-xSplinePos,0));
        spriteController.spline.SetPosition(1, new Vector3(xSplinePos, 0));

        // Set Collider Sizes and Positions
        Vector2 colliderSize = new Vector2((totalWidth - GapWidth) / 2, 0.5f);
        Vector2 colliderOffset = new Vector2((colliderSize.x + GapWidth) / 2, 0);

        colliders[0].size = colliderSize;
        colliders[1].size = colliderSize;

        colliders[0].offset = -colliderOffset;
        colliders[1].offset = colliderOffset;
    }

    /// <summary>
    /// Handles the switching between states as well as the Layer and Sorting Layer changes needed.
    /// </summary>
    /// <param name="newState">New state of the Disk</param>
    private void SetState(DiskState newState)
    {
        State = newState;

        if (State == DiskState.Stacked || State == DiskState.Stacking)
        {
            gameObject.layer = LayerMask.NameToLayer(LayerUtility.Stacked);
            spriteShapeRenderer.sortingLayerName = SortingLayerUtility.Peg;
        }else if (State == DiskState.Unstacked)
        {
            gameObject.layer = LayerMask.NameToLayer(LayerUtility.Unstacked);
            spriteShapeRenderer.sortingLayerName = SortingLayerUtility.Default;
        }
        else if (State == DiskState.Obstacle)
        {
            gameObject.layer = LayerMask.NameToLayer(LayerUtility.Obstacle);
            spriteShapeRenderer.sortingLayerName = SortingLayerUtility.Default;
        }else if (State == DiskState.Carried)
        {
            gameObject.layer = LayerMask.NameToLayer(LayerUtility.Carried);
            spriteShapeRenderer.sortingLayerName = SortingLayerUtility.Default;
        }

    }
    
    // Player Carry Functions
    public void CarryDisk()
    {
        if(State == DiskState.Obstacle)
        {
            SetState(DiskState.Carried);
        }
    }
    public void ThrowDisk()
    {
        if(State == DiskState.Carried)
        {
            SetState(DiskState.Obstacle);
        }
    }

    // Peg functions
    public Peg FindPeg()
    {
        Peg foundPeg = null;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.1f, LayerMask.GetMask(new[] { LayerUtility.Peg }));
        foreach (Collider2D collider in colliders)
        {
            foundPeg = collider.GetComponent<Peg>();
            if (foundPeg != null) break;
        }

        return foundPeg;
    }
    public void ForceUnstackDisk()
    {
        SetState(DiskState.Unstacked);
        OnUnstack.Invoke(this);
    }

    // Used to check if the disk has entered a peg.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Peg peg = collision.GetComponent<Peg>();
        PegSensor pegSensor = collision.GetComponent<PegSensor>();

        if (peg != null || pegSensor != null)
        {
            peg = peg == null ? pegSensor.GetComponentInParent<Peg>() : peg;
            pegSensor = pegSensor == null ? peg.GetComponentInChildren<PegSensor>() : pegSensor;

            pegTriggerCount++;
            if (pegTriggerCount <= 1 && State == DiskState.Obstacle)
            {
                SetState(DiskState.Stacking);
                Debug.Log(gameObject.name + " has entered the " + peg.type.ToString() + " peg.");
            }
        }
    }

    // Used to check if the disk has exited a peg.
    private void OnTriggerExit2D(Collider2D collision)
    {
        Peg peg = collision.GetComponent<Peg>();
        PegSensor pegSensor = collision.GetComponent<PegSensor>();

        if (peg != null || pegSensor != null)
        {
            peg = peg == null ? pegSensor.GetComponentInParent<Peg>() : peg;
            pegSensor = pegSensor == null ? peg.GetComponentInChildren<PegSensor>() : pegSensor;

            pegTriggerCount--;
            if (pegTriggerCount <= 0)
            {
                if (State == DiskState.Stacked)
                {
                    SetState(DiskState.Unstacked);
                    if (this == CurrentPeg.PeekDisk())
                        CurrentPeg.Pop();
                    else
                    {
                        CurrentPeg.ManualRemoveDisk(this);
                    }
                    OnUnstack.Invoke(this);
                }
                else if (State == DiskState.Unstacked || State == DiskState.Stacking)
                    SetState(DiskState.Obstacle);

                Debug.Log(gameObject.name + " has exited the " + peg.type.ToString() + " peg.");
            }
        }
    }

    // Used to confirm whether the disk has been successfully stacked onto a peg.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        string colliderLayer = LayerMask.LayerToName(collision.gameObject.layer);
        
        if (State == DiskState.Unstacked && colliderLayer == LayerUtility.Platform)
        {
            SetState(DiskState.Obstacle);
            Debug.Log(gameObject.name + " was unstacked after colliding with " + collision.gameObject.name);
        }
        if (State == DiskState.Stacking && pegTriggerCount > 0 && (colliderLayer == LayerUtility.Platform || collision.gameObject.TryGetComponent<Disk>(out Disk otherDisk)))
        {
            Peg foundPeg = FindPeg();
            if (foundPeg == null)
                Debug.LogError("Something went wrong, when stacking " + gameObject.name + ". No peg was found.");
            else
            {
                Debug.Log(gameObject.name + " was stacked into " + foundPeg.name);
                SetState(DiskState.Stacked);
                if (foundPeg.PushDisk(this))
                    OnStack.Invoke(this);
            }
        }

        if(collision.relativeVelocity.sqrMagnitude > 25)
        {
            audioManager.PlayRandomFromAudioList(AudioListName.Disk, audioSource);
        }
    }
}