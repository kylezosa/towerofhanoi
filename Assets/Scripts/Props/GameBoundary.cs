﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoundary : MonoBehaviour
{
    private GameManager gameManager;
    void Start()
    {
        gameManager = Singleton<GameManager>.Instance;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player)) gameManager.OutOfBounds("Player");
        else if (collision.TryGetComponent<Disk>(out Disk disk)) gameManager.OutOfBounds("Disk");
        else if (LayerMask.LayerToName(collision.gameObject.layer) == LayerUtility.Platform) return; // Do nothing
        else Destroy(collision.gameObject);
    }
}
