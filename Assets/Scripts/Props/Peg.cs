﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.Events;

public class Peg : MonoBehaviour
{
    [Min(3)] public float Height = 3f;
    public PegType type;
    [SerializeField] public List<Disk> disks = new List<Disk>();


    //Events
    // Peg: this, Disk: disk involved in the operation, bool: if operation was a success
    public UnityEvent<Peg, Disk, bool> OnPushDisk;
    public UnityEvent<Peg, Disk, bool> OnPopDisk;

    // Graphics and Physics
    private Rigidbody2D rb;
    private SpriteShapeController spriteController;

    // Physics Colliders
    private CircleCollider2D tipCollider;
    private BoxCollider2D poleCollider;

    // Trigger Collider
    private CircleCollider2D sensorCollider;

    private GameManager gameManager;
    private AudioManager audioManager;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteController = GetComponent<SpriteShapeController>();

        poleCollider = GetComponent<BoxCollider2D>();
        foreach(CircleCollider2D circleCollider in GetComponentsInChildren<CircleCollider2D>())
        {
            if (circleCollider.gameObject.Equals(gameObject))
                tipCollider = circleCollider;
            else
                sensorCollider = circleCollider;
        }

        gameManager = Singleton<GameManager>.Instance;
        audioManager = Singleton<AudioManager>.Instance;

        Initialize();
    }

    public void Initialize()
    {
        UpdateSize();
        disks = ManualCheckStack();
    }

    /// <summary>
    /// Updates the dimensions of the Peg and its colliders based on the height.
    /// </summary>
    public void UpdateSize()
    {
        // Dimensions
        float width = 0.5f;

        // Set Sprite Sizes
        spriteController.spline.SetPosition(0, new Vector3(0, Height - 1f));
        spriteController.spline.SetPosition(1, new Vector3(0, 1f));

        // Set Physics Collider Sizes and Positions
        Vector2 circleColliderOffset = new Vector2(0, Height - width / 2);

        tipCollider.radius = width / 2;
        tipCollider.offset = circleColliderOffset;

        poleCollider.offset = circleColliderOffset / 2;
        poleCollider.size = new Vector2(width, circleColliderOffset.y);

        // Set Trigger Collider Sizes and Positions
        sensorCollider.radius = width / 2;
        sensorCollider.offset = new Vector2(0, Height - sensorCollider.radius);
    }

    public int PeekSize() => disks.Count > 0 ? disks[disks.Count-1].Size : int.MaxValue;
    public Disk PeekDisk()
    {
        try
        {
            return disks[disks.Count-1];
        }
        catch (System.InvalidOperationException)
        {
            return null;
        }
    }
    public bool Push(Disk disk)
    {
        if (PeekSize() > disk.Size)
        {
            disks.Add(disk);
            return true;
        }
        else return false;
    }
    public Disk Pop()
    {
        if (disks.Count > 0)
        {
            Disk disk = disks[disks.Count - 1];
            disks.RemoveAt(disks.Count - 1);
            return disk;
        }
        return null;
    }

    public List<Disk> ManualCheckStack()
    {
        List<Disk> foundDisks = new List<Disk>();

        float diskHeight = 0.5f;
        Vector2 offset = transform.up * diskHeight;
        Vector2 checkPosition = (Vector2) transform.position + offset / 2;

        for(int i = 0; i < Mathf.Ceil(Height * 2); i++)
        {
            Collider2D collider = Physics2D.OverlapCircle(checkPosition, 0.1f, LayerMask.GetMask(new[] { LayerUtility.Stacked }));
            if (collider != null && collider.TryGetComponent<Disk>(out Disk disk))
                foundDisks.Add(disk); // PushDisk(disk);

            checkPosition += offset;
        }

        return foundDisks;
    }
    public bool PushDisk(Disk disk)
    {
        // Push disk to stack, if invalid, reject disk.
        bool validPush = Push(disk);
        if (validPush)
        {
            audioManager.PlayRandomFromAudioList(AudioListName.PositivePush);
            Debug.Log(gameObject.name + " Push has accepted " + disk.name);
        }
        else
        {
            Debug.Log(gameObject.name + " Push has rejected " + disk.name);
            EjectDisk(disk);
        }
        OnPushDisk.Invoke(this, disk, validPush);
        return validPush;
    }
    public void PopDisk()
    {
        Disk disk = Pop();
        Debug.Log(gameObject.name + " Popped " + disk.name);
        OnPopDisk.Invoke(this, disk, disk != null);
    }
    public void ManualRemoveDisk(Disk disk) { if (disks.Contains(disk)) disks.Remove(disk); }
    public void EjectDisk(Disk disk) => EjectDisk(disk, new Vector2(Random.Range(-1,1f) , Mathf.Sqrt(2 * (Height + 2.0f) * -Physics2D.gravity.y)));
    public void EjectDisk(Disk disk, Vector2 ejectionVelocity)
    {
        Rigidbody2D diskRB = disk.GetComponent<Rigidbody2D>();
        diskRB.velocity = ejectionVelocity;
        diskRB.angularVelocity = Random.Range(-200f,200f);
        audioManager.PlayAudio("Pop 01", AudioType.SFX);
        disk.ForceUnstackDisk();
    }

    void OnDestroy()
    {
        OnPushDisk.RemoveAllListeners();
        OnPopDisk.RemoveAllListeners();
    }
}