﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneUtility
{
    public const string MainMenu = "Main Menu";
    public const string Game = "Game";
    public const string GameOver = "Game Over";
}
