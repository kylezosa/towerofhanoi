﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SortingLayerUtility
{
    public const string Background = "Background";
    public const string Peg = "Peg";
    public const string Default = "Default";
    public const string UI = "UI";
}
