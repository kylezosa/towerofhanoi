﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private CameraManager cameraManager;
    public Vector2 parallax = new Vector2(1.0f, 0.2f);
    private Vector2 startPos;
    void Start()
    {
        cameraManager = Singleton<CameraManager>.Instance;
        startPos = transform.position;
    }
    private void LateUpdate()
    {
        Vector2 newPos = startPos + (cameraManager.main.transform.position * parallax);
        transform.position = newPos;
    }
}
