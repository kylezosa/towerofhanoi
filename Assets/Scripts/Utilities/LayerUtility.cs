﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LayerUtility
{
    public const string Platform = "Platform";
    public const string Peg = "Peg";
    public const string PegSensor = "PegSensor";
    public const string Others = "Others";
    public const string Player = "Player";
    public const string Stacked = "Stacked";
    public const string Unstacked = "Unstacked";
    public const string Obstacle = "Obstacle";
    public const string Carried = "Carried";
}
