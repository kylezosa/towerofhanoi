﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Movement Variables
    public float movementSpeed = 5.0f;
    public float jumpForce = 5.0f;

    // Pick up Variables
    public float pickupRadius = 0.5f;
    public float carryRadius = 1.5f;

    public Disk carriedDisk = null;

    // Pick up tether
    private Rigidbody2D hand = null;
    private DistanceJoint2D handJoint = null; // Used to tether the picked up object to the hand

    private Vector2 handOffset = new Vector2(0, 1.5f); // Places the the hand at the center of the player sprite

    public float minimumThrowForce = 50f;
    public float maximumThrowForce = 100f;
    private float chargedForce = 50f;

    private Action Interact;

    // Internal movement variables
    private Rigidbody2D rb;

    private float xIn = 0;
    private Vector3 internalAcc = Vector3.zero;
    private float moveSmoothTime = 0.1f;

    private bool isJumping = false;

    private Animator animator;
    private SpriteRenderer spriteRenderer;

    // Manager references
    private GameManager gameManager;
    private CameraManager cameraManager;
    private AudioManager audioManager;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        gameManager = Singleton<GameManager>.Instance;
        cameraManager = Singleton<CameraManager>.Instance;
        audioManager = Singleton<AudioManager>.Instance;

        foreach(Rigidbody2D component in GetComponentsInChildren<Rigidbody2D>())
        {
            if (!gameObject.Equals(component.gameObject))
            {
                hand = component;
                handJoint = hand.GetComponent<DistanceJoint2D>();
                break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        xIn = Input.GetAxisRaw("Horizontal");
        Vector2 handCenter = (Vector2) transform.position + handOffset;
        hand.transform.position = handCenter + Vector2.ClampMagnitude((Vector2) cameraManager.main.ScreenToWorldPoint(Input.mousePosition) - handCenter, carryRadius);

        if (Input.GetMouseButtonDown(0))
        {
            Vector2 direction = cameraManager.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            ThrowDisk(direction.normalized * maximumThrowForce);
        }
        if (Input.GetButtonDown("Jump") && !isJumping) Jump(jumpForce);
        if (Input.GetKeyDown(KeyCode.E) && Interact != null) Interact();

        //
    }
    void FixedUpdate()
    {
        MoveHorizontally(xIn * movementSpeed);
    }

    /// <summary>
    /// Moves the player horizontally, ignoring any movement along the y-axis.
    /// </summary>
    /// <param name="xSpeed">Speed along the x-axis.</param>
    /// <param name="capped">Caps the speed by the Player's Movement Speed property.</param>
    void MoveHorizontally(float xSpeed, bool capped = true)
    {
        animator.SetFloat("Speed", Mathf.Abs(xSpeed));
        if (xSpeed > 0) spriteRenderer.flipX = false;
        else if (xSpeed < 0) spriteRenderer.flipX = true;

        if (xSpeed == 0) return;

        xSpeed = capped ? Mathf.Clamp(xSpeed, -movementSpeed, movementSpeed) : xSpeed;
        // Ignore Y speed
        Vector3 targetVelocity = new Vector3(xSpeed, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref internalAcc, moveSmoothTime, movementSpeed * 10);
    }
    
    /// <summary>
    /// Applys a vertical jumping force
    /// </summary>
    /// <param name="force">Strength of jumping force</param>
    /// <param name="capped">Caps the jump force by the Player's Jump Force property</param>
    void Jump(float force, bool capped = true)
    {
        force = capped ? Mathf.Clamp(force, -jumpForce, jumpForce) : force;
        rb.AddForce(Vector2.up * rb.mass * force, ForceMode2D.Impulse);

        animator.SetTrigger("Jump");
        isJumping = true;
    }

    /// <summary>
    /// Finds the disk nearest to the Player's hands. Returns null if none is found.
    /// </summary>
    /// <returns></returns>
    Disk FindDiskNearHand()
    {
        Disk closestDisk = null;
        float colliderDist = float.MaxValue;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(hand.transform.position, pickupRadius, LayerMask.GetMask(new[] { LayerUtility.Obstacle }));
        foreach (Collider2D collider in colliders)
        {
            if (collider.TryGetComponent<Disk>(out Disk disk))
            {
                float dist = Vector2.SqrMagnitude(disk.transform.position - hand.transform.position);
                if (closestDisk == null || colliderDist > dist)
                {
                    closestDisk = disk;
                    colliderDist = dist;
                }
            }
        }
        return closestDisk;
    }

    /// <summary>
    /// Finds and equips the disk nearest to player hand.
    /// </summary>
    public void PickUpDisk()
    {
        if (carriedDisk == null)
        {
            Disk foundDisk = FindDiskNearHand();
            if (foundDisk != null)
            {
                CarryDisk(foundDisk);
                animator.SetTrigger("PickUp");
                audioManager.PlayRandomFromAudioList(AudioListName.PickUp);
            }

            Interact = DropDisk;
        }
    }

    /// <summary>
    /// Throws disk with zero force
    /// </summary>
    public void DropDisk()
    {
        ThrowDisk(Vector2.zero);
        Interact = PickUpDisk;
    }

    /// <summary>
    /// 
    /// </summary>
    public void ActivatePeg()
    {
        Peg closestPeg = null;
        float colliderDist = float.MaxValue;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(hand.transform.position, pickupRadius, LayerMask.GetMask(new[] { LayerUtility.Peg }));

        foreach (Collider2D collider in colliders)
        {
            if (collider.TryGetComponent<Peg>(out Peg peg))
            {
                float dist = Vector2.SqrMagnitude(peg.transform.position - hand.transform.position);
                if (closestPeg == null || colliderDist > dist)
                {
                    closestPeg = peg;
                    colliderDist = dist;
                }
            }
        }

        if(closestPeg != null)
        {
            closestPeg.PopDisk();
        }
    }

    public void AllowCarrying() => Interact = PickUpDisk;
    public void AllowPegActivation() => Interact = ActivatePeg;

    /// <summary>
    /// Places the disk in a Carry state and attaches it to the player's hands.
    /// </summary>
    /// <param name="disk"></param>
    public void CarryDisk(Disk disk)
    {
        if(carriedDisk == null)
        {
            carriedDisk = disk;

            Rigidbody2D carriedRB = carriedDisk.GetComponent<Rigidbody2D>();
            carriedRB.WakeUp();
            carriedRB.gravityScale = 0;
            handJoint.connectedBody = carriedRB;

            carriedDisk.CarryDisk();
            Debug.Log(gameObject.name + " picked up " + carriedDisk.name);
        }
        else
        {
            Debug.Log(gameObject.name + " is already holding " + carriedDisk.name);
        }
    }

    /// <summary>
    /// Throws the disk currently being held by the player.
    /// </summary>
    /// <param name="throwForce">Force to be applied to the disk.</param>
    /// <param name="capped">Cap the force by the Player min/max force parameters?</param>
    void ThrowDisk(Vector2 throwForce, bool capped = true)
    {
        if(carriedDisk != null)
        {
            Rigidbody2D diskRB = carriedDisk.GetComponent<Rigidbody2D>();

            diskRB.AddForce(throwForce, ForceMode2D.Impulse);
            diskRB.gravityScale = 1;

            carriedDisk.ThrowDisk();
            Debug.Log(gameObject.name + " threw " + carriedDisk.name);
            carriedDisk = null;
            handJoint.connectedBody = null;

            Interact = PickUpDisk;

            if(throwForce != Vector2.zero)
            {
                animator.SetTrigger("Throw");
                spriteRenderer.flipX = throwForce.x < 0;
                audioManager.PlayRandomFromAudioList(AudioListName.Throw);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (isJumping && LayerMask.LayerToName(collision.gameObject.layer) == LayerUtility.Platform)
            isJumping = false;
    }
}
