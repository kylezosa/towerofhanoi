﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public int NumberOfDisks = 3;

    float timeElapsed = 0;
    float timeLimit = 300;

    public Peg Source, Auxiliary, Destination;
    [SerializeField] public List<Disk> Disks;

    public Disk looseDisk = null;
    public Player Player;

    private PauseMenu pauseMenu;
    private AudioManager audioManager;

    [SerializeField] private GameObject diskPrefab;

    public void Start()
    {
        NumberOfDisks = MainMenu.NumberOfDisks;
        audioManager = Singleton<AudioManager>.Instance;
        audioManager.PlayAudio(AudioUtility.GameMusic01, AudioType.BGM);
        audioManager.PlayAudio("Forest Wind", AudioType.Ambience);
        audioManager.PlayAudio("Rain", AudioType.Ambience);
        audioManager.PlayAudio("Wind", AudioType.Ambience);

        Player.AllowPegActivation();

        Source.OnPushDisk.AddListener(ValidatePegPush);
        Source.OnPopDisk.AddListener(ValidatePegPop);

        Auxiliary.OnPushDisk.AddListener(ValidatePegPush);
        Auxiliary.OnPopDisk.AddListener(ValidatePegPop);

        Destination.OnPushDisk.AddListener(ValidatePegPush);
        Destination.OnPopDisk.AddListener(ValidatePegPop);

        pauseMenu = Singleton<PauseMenu>.Instance;
        Initialize();
    }
    public void Update()
    {
    }

    public void Initialize()
    {
        timeElapsed = 0;

        // Reset all Pegs

        // Create NumberOfDisks sorted at the Source peg.
        int offsetCounter = 0;

        for (int i = NumberOfDisks; i > 0; i--)
        {
            Vector2 newPosition = Source.transform.position;
            newPosition.y = newPosition.y + 0.25f + offsetCounter * 0.5f;

            GameObject newGO = Instantiate(diskPrefab, newPosition, Quaternion.identity);
            Disk disk = newGO.GetComponent<Disk>();

            disk.Size = i;
            // disk.UpdateSize();
            offsetCounter++;
        }
        Source.disks = Source.ManualCheckStack();
    }

    public void ValidatePegPush(Peg peg, Disk disk, bool success)
    {
        if (success)
        {
            if (!ValidatePeg(peg))
            {
                Debug.Log("Game Manager has found " + peg.name + " stack to be unsorted and invalid.");
                audioManager.PlayAudio("Negative Pop 01", AudioType.SFX);
            }
            else if (CheckWin())
            {
                Debug.Log("Game Manager declares the game has been won!");

                audioManager.PlayRandomFromAudioList(AudioListName.PositivePush);
                audioManager.StopBGM();
                audioManager.StopAmbience();
                audioManager.PlayAudio("Win", AudioType.SFX);

                pauseMenu.OpenGameOver("YOU WIN!");
            }
            else
            {
                if (looseDisk != disk)
                    Debug.LogWarning("Game Manager: Somehow a non-loose disk was pushed to a peg. How?");

                disk.SetPeg(peg);
                looseDisk = null;
                Player.AllowPegActivation();
            }
        }
    }
    public void ValidatePegPop(Peg peg, Disk disk, bool success)
    {
        if (success)
        {
            if(looseDisk == null)
            {
                peg.EjectDisk(disk);
                looseDisk = disk;
                Player.AllowCarrying();
                Debug.Log("Game Manager has validated " + peg.name + "'s pop of " + disk.name);
            }
            else
            {
                peg.Push(disk);
                Player.AllowPegActivation();
                Debug.Log("Game Manager has rejected " + peg.name + "'s pop of " + disk.name);
            }
        }
    }

    public bool ValidatePeg(Peg peg)
    {
        bool valid = true;
        int compare = int.MaxValue;

        foreach (Disk listedDisk in peg.disks)
        {
            if (listedDisk.Size > compare)
            {
                valid = false;
                break;
            }
            compare = listedDisk.Size;
        }
        return valid;
    }
    public bool CheckWin() => Destination.disks.Count == NumberOfDisks && ValidatePeg(Destination); // && disks.IsSorted
    public bool CheckLose() => timeElapsed >= timeLimit; // If time limit is required
    public void OutOfBounds(string reason)
    {
        Debug.Log("Game Manager declares the game has been lost! " + reason + " went out of bounds!");
        audioManager.StopBGM();
        audioManager.StopAmbience();
        audioManager.PlayAudio("Lose", AudioType.SFX);
        pauseMenu.OpenGameOver("YOU LOSE!");
    }
    // Should check for health? If health mechanic should be added..?
}
