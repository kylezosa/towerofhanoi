﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// AudioClip , AudioSource

public enum AudioType { BGM, Ambience, SFX };
public enum AudioListName { Disk, Throw, PositivePush, PickUp };
public class AudioManager : Singleton<AudioManager>
{
    public List<AudioClip> AudioClips = new List<AudioClip>();
    private Dictionary<string, AudioClip> AudioClipDictionary = new Dictionary<string, AudioClip>();

    private List<AudioClip> DiskImpactAudio = new List<AudioClip>();
    private List<AudioClip> ThrowAudio = new List<AudioClip>();
    private List<AudioClip> PositivePushAudio = new List<AudioClip>();
    private List<AudioClip> PickUpAudio = new List<AudioClip>();

    public AudioSource BGMAudioSource;
    public List<AudioSource> AmbienceAudioSources = new List<AudioSource>();
    public List<AudioSource> SFXAudioSources = new List<AudioSource>();

    private int ambienceCounter = 0;
    private int sfxCounter = 0;

    private static AudioManager instance;
    void Awake()
    {
        if (instance != null) Destroy(this.gameObject);
    }

    void Start()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);

        BGMAudioSource.loop = true;

        foreach(AudioSource ambienceAudioSource in AmbienceAudioSources)
        {
            ambienceAudioSource.loop = false;
        }
        foreach (AudioSource sfxAudioSource in SFXAudioSources)
        {
            sfxAudioSource.loop = false;
        }
        foreach (AudioClip clip in AudioClips)
        {
            AudioClipDictionary.Add(clip.name, clip);
            if (clip.name.Contains("Disk Hit")) DiskImpactAudio.Add(clip);
            else if (clip.name.Contains("Throw")) ThrowAudio.Add(clip);
            else if (clip.name.Contains("Positive Push")) PositivePushAudio.Add(clip);
            else if (clip.name.Contains("PickUp")) PickUpAudio.Add(clip);
        }
    }

    private AudioSource GetAmbienceAudioSource()
    {
        AudioSource freeAudioSource = null;

        foreach (AudioSource ambienceAudioSource in AmbienceAudioSources)
        {
            if (!ambienceAudioSource.isPlaying)
            {
                freeAudioSource = ambienceAudioSource;
                break;
            }
        }

        return freeAudioSource == null ? AmbienceAudioSources[ambienceCounter++ % AmbienceAudioSources.Count] : freeAudioSource;
    }
    private AudioSource GetSFXAudioSource()
    {
        AudioSource freeAudioSource = null;

        foreach (AudioSource sfxAudioSource in SFXAudioSources)
        {
            if (!sfxAudioSource.isPlaying)
            {
                freeAudioSource = sfxAudioSource;
                break;
            }
        }

        return freeAudioSource == null ? SFXAudioSources[sfxCounter++ % SFXAudioSources.Count] : freeAudioSource;
    }

    public void PlayAudio(string name, AudioType type)
    {
        Debug.Log("Playing " + name + " of type " + type);
        AudioSource audioSource;
        if (type == AudioType.BGM )
        {
            if (BGMAudioSource.clip != null && BGMAudioSource.isPlaying && BGMAudioSource.clip.name == name) return;
            else audioSource = BGMAudioSource;
        }
        else if (type == AudioType.Ambience) audioSource = GetAmbienceAudioSource();
        else audioSource = GetSFXAudioSource();

        audioSource.clip = AudioClipDictionary[name];
        audioSource.Play();
    }

    public void PlayAudio(string name, AudioSource source)
    {
        Debug.Log("Playing " + name + " at " + source.name);
        source.clip = AudioClipDictionary[name];
        source.Play();
    }

    public void PlayRandomFromAudioList(AudioListName listName, AudioSource sfxAudioSource = null)
    {
        sfxAudioSource = sfxAudioSource == null ? GetSFXAudioSource() : sfxAudioSource;

        if (listName == AudioListName.Disk) sfxAudioSource.clip = DiskImpactAudio[Random.Range(0, DiskImpactAudio.Count - 1)];
        else if (listName == AudioListName.Throw) sfxAudioSource.clip = ThrowAudio[Random.Range(0, ThrowAudio.Count - 1)];
        else if (listName == AudioListName.PositivePush) sfxAudioSource.clip = PositivePushAudio[Random.Range(0, PositivePushAudio.Count - 1)];
        else if (listName == AudioListName.PickUp) sfxAudioSource.clip = PickUpAudio[Random.Range(0, PickUpAudio.Count - 1)];
        else {
            Debug.LogWarning(listName + " not found");
            return;
        }

        sfxAudioSource.Play();
    }
    public void StopBGM() => BGMAudioSource.Pause();

    public void StopAmbience()
    {
        foreach(AudioSource ambienceAudioSource in AmbienceAudioSources) ambienceAudioSource.Stop();
    }
}

public static class AudioUtility
{
    public const string ButtonOnHover = "Button On Hover";
    public const string ButtonOnClick = "Button On Click";
    public const string MainMenuBGM = "Main Menu";
    public const string GameMusic01 = "Game Music Loop";
    public const string GameMusic02 = "Game Music Loop Percussion";
}