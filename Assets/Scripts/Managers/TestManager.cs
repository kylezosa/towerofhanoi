﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class TestManager : Singleton<TestManager>
{
    public float forceStrength = 10;
    public float explosionRadius = 2.5f;
    public Camera cam;

    public SpriteShapeController sprShape;

    void Start()
    {
    }
    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    Explode(cam.ScreenToWorldPoint(Input.mousePosition));
        //}
    }

    /// <summary>
    /// Testing with Physics
    /// </summary>
    public void Explode(Vector2 origin)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(origin, explosionRadius);
        foreach(Collider2D collider in colliders)
        {
            // Debug.Log(collider.TryGetComponent<Rigidbody2D>(out Rigidbody2D rr));
            if(collider.TryGetComponent<Rigidbody2D>(out Rigidbody2D rb))
            {
                //Vector2 position = (rb.position + collider.ClosestPoint(origin)) / 2;
                //Vector2 force = (position - origin).normalized * forceStrength;
                // rb.AddForceAtPosition(force, position, ForceMode2D.Impulse);
                Vector2 force = (rb.position - origin).normalized * forceStrength;
                rb.AddForce(force, ForceMode2D.Impulse);
            }
        }
    }
}
