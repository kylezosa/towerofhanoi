﻿/// <summary>
/// Used to determine which objects should be able to collide with the Disk
/// </summary>
public enum DiskState { Stacking, Stacked, Unstacked, Obstacle, Carried }