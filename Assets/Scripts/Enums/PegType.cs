﻿/// <summary>
/// Used to determine disk transfer win condition
/// </summary>
public enum PegType { Source, Auxillary, Destination }