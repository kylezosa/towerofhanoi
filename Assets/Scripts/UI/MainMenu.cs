﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    public static int NumberOfDisks = 3;

    [SerializeField] private Slider diskCountSlider;
    [SerializeField] private TextMeshProUGUI diskCountLabel;
    private AudioManager audioManager;

    private void Start()
    {
        audioManager = Singleton<AudioManager>.Instance;

        audioManager.StopAmbience();
        audioManager.PlayAudio(AudioUtility.MainMenuBGM, AudioType.BGM);
    }


    // For some reason, Main Menu BGM does not play on Build Launch. This is the work around.
    private bool audioTriggered = false;
    private float timeElapsed = 0f;
    private void Update()
    {
        if(1.5f > timeElapsed)
        {
            timeElapsed += Time.deltaTime;
        }else if (!audioTriggered)
        {
            audioTriggered = true;
            audioManager.PlayAudio(AudioUtility.MainMenuBGM, AudioType.BGM);
        }
    }
    public void SetNumberOfDisks()
    {
        NumberOfDisks = (int)diskCountSlider.value;
        diskCountLabel.text = "NUMBER OF DISKS - " + NumberOfDisks;
    }

    public void StartGame()
    {
        Debug.Log("Starting game.");
        audioManager.PlayAudio("Start Game", AudioType.SFX);
        SceneManager.LoadScene(SceneUtility.Game);
    }

    public void Exit()
    {
        Debug.Log("Exiting game.");
        Application.Quit();
    }
}