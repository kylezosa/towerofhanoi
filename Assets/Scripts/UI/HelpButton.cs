﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpButton : MonoBehaviour
{
    [SerializeField] private GameObject HelpMenu;
    private void Start()
    {
        HelpMenu.SetActive(false);
    }
}
