﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class MenuButton : MonoBehaviour
{
    private AudioManager audioManager;
    private EventTrigger eventTrigger;
    private EventTrigger.Entry onHover = new EventTrigger.Entry();
    private EventTrigger.Entry onClick = new EventTrigger.Entry();

    void Start()
    {
        audioManager = Singleton<AudioManager>.Instance;
        eventTrigger = GetComponent<EventTrigger>();

        onHover.eventID = EventTriggerType.PointerEnter;
        onHover.callback.AddListener((eventData) => { audioManager.PlayAudio(AudioUtility.ButtonOnHover, AudioType.SFX); });

        onClick.eventID = EventTriggerType.PointerDown;
        onClick.callback.AddListener((eventData) => { audioManager.PlayAudio(AudioUtility.ButtonOnClick, AudioType.SFX); });

        eventTrigger.triggers.Add(onHover);
        eventTrigger.triggers.Add(onClick);
    }
}
