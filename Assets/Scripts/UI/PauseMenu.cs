﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PauseMenu : Singleton<PauseMenu>
{
    public static bool GameIsPaused = false;

    private float originalTimeScale = 1f;

    [SerializeField] private Canvas UICanvas;
    [SerializeField] private GameObject PauseScreen;
    [SerializeField] private GameObject GameOverScreen;
    [SerializeField] private TextMeshProUGUI GameOverText;

    public void Start()
    {
        Resume();
        UICanvas.gameObject.SetActive(false);
        PauseScreen.SetActive(false);
        GameOverScreen.SetActive(false);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused) ClosePauseMenu();
            else OpenPauseMenu();
        }
    }
    public void Pause()
    {
        originalTimeScale = Time.timeScale;
        Time.timeScale = 0;
    }
    public void Resume()
    {
        Time.timeScale = originalTimeScale;
    }
    public void OpenPauseMenu()
    {
        Pause();
        UICanvas.gameObject.SetActive(true);
        PauseScreen.SetActive(true);
        GameIsPaused = true;
    }

    public void ClosePauseMenu()
    {
        Resume();
        UICanvas.gameObject.SetActive(false);
        PauseScreen.SetActive(false);
        GameIsPaused = false;
    }

    public void OpenGameOver(string message = "YOU WIN!")
    {
        Pause();
        UICanvas.gameObject.SetActive(true);
        GameOverScreen.SetActive(true);
        GameOverText.text = message;
    }
    public void Restart()
    {
        Debug.Log("Restarting game.");
        ClosePauseMenu();
        SceneManager.LoadScene(SceneUtility.Game);
    }
    public void ReturnToMainMenu()
    {
        Debug.Log("Returning to Main Menu.");
        ClosePauseMenu();
        SceneManager.LoadScene(SceneUtility.MainMenu);
    }
    public void Exit()
    {
        Debug.Log("Exiting game.");
        ClosePauseMenu();
        Application.Quit();
    }
}
