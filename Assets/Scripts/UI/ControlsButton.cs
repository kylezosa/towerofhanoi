﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsButton : MonoBehaviour
{
    [SerializeField] private GameObject ControlsGraphic;
    private void Start()
    {
        ControlsGraphic.SetActive(false);
    }
    public void Display(bool enable) => ControlsGraphic.SetActive(enable);
}
